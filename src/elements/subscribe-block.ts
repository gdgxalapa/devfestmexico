import { customElement, observe, property } from '@polymer/decorators';
import '@polymer/iron-icon';
import '@polymer/paper-button';
import { html, PolymerElement } from '@polymer/polymer';
import { ReduxMixin } from '../mixins/redux-mixin';
import { RootState, store } from '../store';
import { openDialog } from '../store/dialogs/actions';
import { DIALOGS } from '../store/dialogs/types';
import { subscribe } from '../store/subscribe/actions';
import './hoverboard-icons';
import './shared-styles';

@customElement('subscribe-block')
export class SubscribeBlock extends ReduxMixin(PolymerElement) {
  static get template() {
    return html`
      <style include="shared-styles flex flex-alignment">
        :host {
          display: flex;
          width: 100%;
          background: var(--default-primary-color);
          color: #fff;
          padding: 16px 0;
        }

        .description {
          font-size: 24px;
          line-height: 1.5;
          margin: 0 0 16px;
        }

        .link-form {
          color: #fff;
        }

        .link-form[disabled] {
          background: var(--default-primary-color);
          color: #fff;
        }

        @media (min-width: 640px) {
          :host {
            padding: 32px 0;
          }

          .description {
            font-size: 32px;
            margin: 0 0 24px;
            text-align: center;
          }
        }
      </style>

      <div class="container" layout vertical center$="[[viewport.isTabletPlus]]">
        <div class="description">{$ subscribeBlock.callToAction.description $}</div>
        <div class="cta-button">
          <a
            class="link-form"
            href="https://docs.google.com/forms/d/1GrZ5xB70ZRapb8EhBHT_I8bX3cs8NaQD_G745OAXtWQ/edit"
            target="_blank"
            rel="noopener noreferrer"
            ga-on="click"
            ga-event-category="ticket button"
            ga-event-action="buy_click"
            layout
            horizontal
            center
          >
            <span class="cta-label">[[ctaLabel]]</span>
            <iron-icon icon$="hoverboard:[[ctaIcon]]"></iron-icon>
          </a>
        </div>
      </div>
    `;
  }

  @property({ type: Object })
  private user: { signedIn?: boolean; email?: string; displayName?: string } = {};
  @property({ type: Object })
  private viewport = {};
  @property({ type: Boolean })
  private subscribed = false;
  @property({ type: String })
  private ctaIcon = 'arrow-right-circle';
  @property({ type: String })
  private ctaLabel = '{$  subscribeBlock.callToAction.label $}';

  stateChanged(state: RootState) {
    this.subscribed = state.subscribed;
    this.user = state.user;
    this.viewport = state.ui.viewport;
  }

  @observe('subscribed')
  _handleSubscribed(subscribed) {
    if (subscribed) {
      this.ctaIcon = 'checked';
      this.ctaLabel = '{$  subscribeBlock.subscribed $}';
    } else {
      this.ctaIcon = 'arrow-right-circle';
      this.ctaLabel = '{$  subscribeBlock.callToAction.label $}';
    }
  }

  _subscribe() {
    let userData: {
      firstFieldValue?: string;
      secondFieldValue?: string;
    } = {};

    if (this.user.signedIn) {
      const fullNameSplit = this.user.displayName.split(' ');
      userData = {
        firstFieldValue: fullNameSplit[0],
        secondFieldValue: fullNameSplit[1],
      };
    }

    if (this.user.email) {
      this._subscribeAction(Object.assign({}, { email: this.user.email }, userData));
    } else {
      openDialog(DIALOGS.SUBSCRIBE, {
        title: '{$ subscribeBlock.formTitle $}',
        submitLabel: ' {$ subscribeBlock.subscribe $}',
        firstFieldLabel: '{$ subscribeBlock.firstName $}',
        secondFieldLabel: '{$ subscribeBlock.lastName $}',
        firstFieldValue: userData.firstFieldValue,
        secondFieldValue: userData.secondFieldValue,
        submit: (data) => this._subscribeAction(data),
      });
    }
  }

  _subscribeAction(data) {
    store.dispatch(subscribe(data));
  }
}
